console.log("heydey")

let courseBooking1 = `[
	{
    	"firstName": "Jane",
    	"lastName": "Doe",
	},
	{
        "firstName": "Stephen",
        "lastName": "Hawking",
    }
]`;

console.log(courseBooking1);

/*
2. 
db.users.find({ 
    $or: [{ firstName: { $regex: 'S', $options: '$i'}},
	   { lastName:  { $regex: 'D', $options: '$i'}}] 
 },{firstName: 1, lastName: 1, _id:0} );
*/

let courseBooking2 = `[
	{
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
            "phone": "87654321",
            "email": "stephenhawking@gmail.com"
        },
        "courses": [ "Python", "React", "PHP" ],
        "department": "HR"
    },
    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
            "phone": "87654321",
            "email": "neilarmstrong@gmail.com"
        },
        "courses": [ "React", "Laravel", "Sass" ],
        "department": "HR"
    }


]`;
console.log(courseBooking2);


/*
3.
db.users.find({
    $and: [{department: "HR"}, {age: {$gte: 30}}]
 });
*/

let courseBooking3 = `[

	{
    	"firstName": "Jane",
    	"lastName": "Doe",
    	"age": 21,
    	"contact": {
        	"phone": "87654321",
        	"email": "janedoe@gmail.com"
    	},
    	"courses": [ "CSS", "Javascript", "Python" ],
    	"department": "HR"
	}

]`;
console.log(courseBooking3);









/*
4.
db.users.find({
    $and: [{ firstName: {$regex: 'e', $options: '$i'}},
            {age: {$lte:30}}]
});

*/